# LIBOBJ - library object.
#
# shell related code.

import argparse
import atexit
import lo
import logging
import logging.handlers
import os
import readline
import sys
import time
import termios
import threading
import traceback

cmds = []
logfiled = ""
resume = {}

class ENOTXT(Exception):
    pass

class Console(lo.hdl.Handler):

    def __init__(self):
        super().__init__()
        self._connected = threading.Event()
        self._ready = threading.Event()
        self._threaded = False

    def announce(self, txt):
        self.raw(txt)

    def poll(self):
        self._connected.wait()
        e = lo.evt.Event()
        e.etype = "command"
        e.origin = "root@shell"
        e.orig = repr(self)
        e.txt = input("> ")
        if not e.txt:
            raise ENOTXT 
        return e

    def input(self):
        while not self._stopped:
            try:
                e = self.poll()
            except ENOTXT:
                continue
            except EOFError:
                break
            self.put(e)
            e.wait()
        self._ready.set()

    def raw(self, txt):
        sys.stdout.write(str(txt) + "\n")
        sys.stdout.flush()

    def say(self, channel, txt, type="chat"):
        self.raw(txt)
 
    def start(self):
        if self.error:
            return
        super().start()
        lo.thr.launch(self.input)
        self._connected.set()

    def wait(self):
        if self.error:
            return
        self._ready.wait()

def close_history():
    global HISTFILE
    if lo.workdir:
        if not HISTFILE:
            HISTFILE = os.path.join(lo.workdir, "history")
        if not os.path.isfile(HISTFILE):
            lo.cdir(HISTFILE)
            lo.touch(HISTFILE)
        readline.write_history_file(HISTFILE)

def complete(text, state):
    matches = []
    if text:
        matches = [s for s in cmds if s and s.startswith(text)]
    else:
        matches = cmds[:]
    try:
        return matches[state]
    except IndexError:
        return None

def enable_history():
    global HISTFILE
    if lo.workdir:
        HISTFILE = os.path.abspath(os.path.join(lo.workdir, "history"))
        if not os.path.exists(HISTFILE):
            lo.touch(HISTFILE)
        else:
            readline.read_history_file(HISTFILE)
    atexit.register(close_history)

def get_completer():
    return readline.get_completer()

def get_exception(txt="", sep=""):
    exctype, excvalue, tb = sys.exc_info()
    trace = traceback.extract_tb(tb)
    result = ""
    for elem in trace:
        fname = elem[0]
        linenr = elem[1]
        func = elem[2]
        plugfile = fname[:-3].split(os.sep)
        mod = []
        for elememt in plugfile[::-1]:
            mod.append(elememt)
            if elememt == "bl":
                break
        ownname = '.'.join(mod[::-1])
        result += "%s:%s %s %s " % (ownname, linenr, func, sep)
    res = "%s%s: %s %s" % (result, exctype, excvalue, str(txt))
    del trace
    return res

def level(loglevel="", logdir="", logfile="losh.log", nostream=False):
    assert lo.workdir
    global logfiled
    if not loglevel:
        loglevel = "error"
    if not logdir:
        logdir = os.path.expanduser(lo.workdir) + os.sep + "logs"
    logfile = logfiled = os.path.join(logdir, logfile)
    if not os.path.exists(logfile):
        lo.cdir(logfile)
        lo.touch(logfile)
    datefmt = '%H:%M:%S'
    format_time = "%(asctime)-8s %(message)-70s"
    format_plain = "%(message)-0s"
    loglevel = loglevel.upper()
    logger = logging.getLogger("")
    if logger.handlers:
        for handler in logger.handlers:
            logger.removeHandler(handler)
    if logger.handlers:
        for handler in logger.handlers:
            logger.removeHandler(handler)
    try:
        logger.setLevel(loglevel)
    except ValueError:
        pass
    formatter = logging.Formatter(format_plain, datefmt)
    if nostream:
        dhandler = DumpHandler()
        dhandler.propagate = False
        dhandler.setLevel(loglevel)
        logger.addHandler(dhandler)
    else:
        handler = logging.StreamHandler()
        handler.propagate = False
        handler.setFormatter(formatter)
        try:
            handler.setLevel(loglevel)
            logger.addHandler(handler)
        except ValueError:
            logging.warning("wrong level %s" % loglevel)
            loglevel = "ERROR"
    formatter2 = logging.Formatter(format_time, datefmt)
    filehandler = logging.handlers.TimedRotatingFileHandler(logfile, 'midnight')
    filehandler.propagate = False
    filehandler.setFormatter(formatter2)
    try:
        filehandler.setLevel(loglevel)
    except ValueError:
        pass
    logger.addHandler(filehandler)
    return logger

def make_opts(ns, options, usage="", **kwargs):
    kwargs["usage"] = usage
    parser = argparse.ArgumentParser(**kwargs)
    for opt in options:
        if not opt:
            continue
        if opt[2] == "store":
            parser.add_argument(opt[0], opt[1], action=opt[2], type=opt[3], default=opt[4], help=opt[5], dest=opt[6], const=opt[4], nargs="?")
        else:
            parser.add_argument(opt[0], opt[1], action=opt[2], default=opt[3], help=opt[4], dest=opt[5])
    parser.add_argument('args', nargs='*')
    parser.parse_known_args(namespace=ns)

def parse_cli(name, version=None, opts=None, usage="", lf=None):
    ns = lo.Object()
    if opts:
        make_opts(ns, opts, usage)
    cfg = lo.Default(ns)
    cfg.name = name
    cfg.txt = " ".join(cfg.args)
    cfg.version = version
    lo.cfg.update(cfg)
    assert cfg.workdir
    lo.workdir = cfg.workdir
    lo.cdir(os.path.join(lo.workdir, "store", ""))
    if lf:
        lo.cdir(lf)
    level(cfg.level, lf) 
    return cfg

def set_completer(commands):
    global cmds
    cmds = commands
    readline.set_completer(complete)
    readline.parse_and_bind("tab: complete")
    atexit.register(lambda: readline.set_completer(None))
        
def setup(fd):
    return termios.tcgetattr(fd)

def termreset():
    if "old" in resume:
        termios.tcsetattr(resume["fd"], termios.TCSADRAIN, resume["old"])

def termsave():
    try:
        resume["fd"] = sys.stdin.fileno()
        resume["old"] = setup(sys.stdin.fileno())
        atexit.register(termreset)
    except termios.error:
        pass    

def touch(fname):
    try:
        fd = os.open(fname, os.O_RDWR | os.O_CREAT)
        os.close(fd)
    except (IsADirectoryError, TypeError):
        pass

def writepid():
    assert lo.workdir
    path = os.path.join(lo.workdir, "pid")
    f = open(path, 'w')
    f.write(str(os.getpid()))
    f.flush()
    f.close()
