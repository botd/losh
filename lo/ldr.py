# LIBOBJ - object library.
#
# module loader.

import cmd
import importlib
import inspect
import lo
import logging
import os
import sys
import types

def __dir__():
    return ("Loader",)

class EINIT(Exception):

    pass

class ENOMODULE(Exception):

    pass

class Loader(lo.Object):

    table = lo.Object()

    def __init__(self):
        super().__init__()
        self.cmds = lo.Object()
        self.error = ""
        
    def direct(self, name):
        logging.warn("direct %s" % name)
        return importlib.import_module(name)

    def find_cmds(self, mod):
        for key, o in inspect.getmembers(mod, inspect.isfunction):
            if "event" in o.__code__.co_varnames:
                if o.__code__.co_argcount == 1:
                    yield (key, o)

    def load_mod(self, mn, force=False):
        if mn in Loader.table:
            return Loader.table[mn]
        mod = None
        if mn in sys.modules:
            mod = sys.modules[mn]
        else:
            try:
                mod = self.direct(mn)
            except ModuleNotFoundError as ex:
                if not mn in str(ex):
                    raise
                return
        if force or mn not in Loader.table:
            Loader.table[mn] = mod
        return Loader.table[mn]

    def walk(self, mns, init=False):
        if not mns:
            return
        mods = []
        for mn in mns.split(","):
            if not mn:
                continue
            m = self.load_mod(mn, False)
            if not m:
                if "." not in mn:
                    mn = "losh.%s" % mn
                    m = self.load_mod(mn, False)
                if not m:
                    continue
            loc = None
            if "__spec__" in dir(m):
                loc = m.__spec__.submodule_search_locations
            if not loc:
                mods.append(m)
                continue
            for md in loc:
                for x in os.listdir(md):
                    if x.endswith(".py"):
                        mmn = "%s.%s" % (mn, x[:-3])
                        m = self.load_mod(mmn, True)
                        if m:
                            mods.append(m)
        for mod in mods:
            cmds = self.find_cmds(mod)
            self.cmds.update(cmds)
            if init and "init" in dir(mod):
                try:
                    mod.init(self)
                except EINIT as ex:
                    self.error = ex
                    break
        return mods
