# BOTD - IRC channel daemon.
#
# data entry.

import lo

def __dir__():
    return ("log", "todo")

class Log(lo.Object):

    def __init__(self):
        super().__init__()
        self.txt = ""

class Todo(lo.Object):

    def __init__(self):
        super().__init__()
        self.txt = ""

def log(event):
    if not event.rest:
       db = lo.dbs.Db()
       nr = 0
       for o in db.find("losh.ent.Log", {"txt": ""}):
            event.display(o, str(nr))
            nr += 1
       return
    obj = Log()
    obj.txt = event.rest
    obj.save()
    event.reply("ok")

def todo(event):
    if not event.rest:
       db = lo.Db()
       nr = 0
       for o in db.find("losh.ent.Todo", {"txt": ""}):
            event.display(o, str(nr))
            nr += 1
       return
    obj = Todo()
    obj.txt = event.rest
    obj.save()
    event.reply("ok")
