# LOSH - librry object shell.
#
# show status information.

import lo
import losh
import os
import threading
import time

def cfg(event):
    assert(lo.workdir)
    if not event.args:
        files = [x.split(".")[-2].lower() for x in os.listdir(os.path.join(lo.workdir, "store")) if x.endswith("Cfg")]
        if files:
            event.reply("choose from %s" % "|".join(["main",] + list(set(files))))
        else:
            event.reply("no configuration files yet.")
        return
    target = event.args[0]
    if target == "main":
        event.reply(lo.cfg)
        return
    cn = "losh.%s.Cfg" % target
    db = lo.dbs.Db()
    l = db.last(cn)
    if not l:     
        event.reply("no %s found." % cn)
        return
    if len(event.args) == 1:
        event.reply(l)
        return
    if len(event.args) == 2:
        event.reply(l.get(event.args[1]))
        return
    setter = {event.args[1]: event.args[2]}
    l.edit(setter)
    l.save()
    event.reply("ok")

def cmds(event):
    cl = []
    for obj in lo.plain.objs:
        if isinstance(obj, lo.hdl.Handler):
            for cmd in obj.cmds:
                if cmd not in cl:
                    cl.append(cmd)
    if cl:
        event.reply(",".join(sorted(cl)))

def objs(event):
    try:
        index = int(event.args[0])
        event.reply(str(lo.plain.objs[index]))
        return
    except (TypeError, ValueError, IndexError):
        pass
    event.reply([lo.typ.get_type(x) for x in lo.plain.objs])

def ps(event):
    psformat = "%-8s %-50s"
    result = []
    for thr in sorted(threading.enumerate(), key=lambda x: x.getName()):
        if str(thr).startswith("<_"):
            continue
        d = vars(thr)
        o = lo.Object()
        o.update(d)
        if o.get("sleep", None):
            up = o.sleep - int(time.time() - o.state.latest)
        else:
            up = int(time.time() - lo.starttime)
        result.append((up, thr.getName(), o))
    nr = -1
    for up, thrname, o in sorted(result, key=lambda x: x[0]):
        nr += 1
        res = "%s %s" % (nr, psformat % (lo.tms.elapsed(up), thrname[:60]))
        if res.strip():
            event.reply(res)

def up(event):
    event.reply(lo.tms.elapsed(time.time() - lo.starttime))

def v(event):
    event.reply("LOSH %s" % losh.__version__)
