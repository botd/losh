# LOSH - lib object shell.
#
# edit configuration. 

import lo
import os

def __dir__():
    return ("cfg", "main") 

def cfg(event):
    assert(lo.workdir)
    if not event.args:
        files = [x.split(".")[-2].lower() for x in os.listdir(os.path.join(lo.workdir, "store")) if x.endswith("Cfg")]
        if files:
            event.reply("choose from %s" % "|".join(set(files)))
        else:
            event.reply("no configuration files yet.")
        return
    target = event.args[0]
    cn = "losh.%s.Cfg" % target
    db = lo.dbs.Db()
    l = db.last(cn)
    if not l:     
        event.reply("no %s found." % cn)
        return
    if len(event.args) == 1:
        event.reply(l)
        return
    if len(event.args) == 2:
        event.reply(l.get(event.args[1]))
        return
    setter = {event.args[1]: event.args[2]}
    l.edit(setter)
    l.save()
    event.reply("ok")

def main(event):
    event.reply(lo.cfg)
