# LOSH - lib object shell.
#
# basic commands.

import lo
import losh

def __dir__():
    return ("cmds", "meet", "users")

def cmds(event):
    cl = []
    for obj in lo.plain.objs:
        print(type(obj))
        if isinstance(obj, lo.hdl.Handler):
            for cmd in obj.cmds:
                if cmd not in cl:
                    cl.append(cmd)
    if cl:
        event.reply(",".join(sorted(cl)))

def meet(event):
    if not event.args:
        event.reply("meet origin [permissions]")
        return
    try:
        origin, *perms = event.args[:]
    except ValueError:
        event.reply("meet origin [permissions]")
        return
    origin = losh.usr.Users.userhosts.get(origin, origin)
    losh.usr.Users().meet(origin, perms)
    event.reply("added %s" % origin)

def users(event):
    res = ""
    db = lo.dbs.Db()
    for o in db.all("botd.usr.User"):
        res += "%s," % o.user
    event.reply(res)
