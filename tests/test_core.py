# LOSH - library object shell.
#
# test load/save

import json
import lo
import os
import unittest

class ENOTCOMPAT(Exception):
    pass

class Test_Core(unittest.TestCase):

    def test_load2(self):
        o = lo.Object()
        o.bla = "mekker"
        p = o.save()
        oo = lo.Object().load(p)
        self.assertEqual(oo.bla, "mekker")

    def test_save(self):
        o = lo.Object()
        p = o.save()
        self.assertTrue(os.path.exists(os.path.join(lo.workdir, "store", p)))

    def test_subitem(self):
        o = lo.Object()
        o.test2 = lo.Object()
        p = o.save()
        oo = lo.Object()
        oo.load(p)
        self.assertTrue(type(oo.test2), lo.Object)

    def test_subitem2(self):
        o = lo.Object()
        o.test = lo.Object()
        o.test.bla = "test"
        p = o.save()
        oo = lo.Object().load(p)
        self.assertTrue(type(oo.test.bla), "test")
