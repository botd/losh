# LOSH - library object shell.
#
# tests attributes on Objects

import lo
import time
import unittest

class Test_Attribute(unittest.TestCase):

    def timed(self):
        with self.assertRaises((AttributeError, )):
            o = lo.Object()
            o.timed2

    def timed2(self):
        o = lo.Object()
        o.date = time.ctime(time.time())
        self.assert_(o.timed())
