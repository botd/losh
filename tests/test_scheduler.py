# LOSH - library object shell
#
# scheduler tests

import lo
import unittest

class Event(lo.evt.Event):

    def show(self):
        if lo.cfg.verbose:
            super().show()

class Test_Scheduler(unittest.TestCase):

    def test_scheduler_put(self):
        h = lo.hdl.Handler()
        h.walk("losh.shw")
        h.start()
        e = Event()
        e.etype = "command"
        e.orig = repr(h)
        e.origin = "root@shell"
        e.txt = "v"
        e.verbose = lo.cfg.verbose
        h.put(e)
        e.wait()
        self.assertTrue(e.result and "LOSH" in e.result[0])
