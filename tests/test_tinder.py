# LOSH - library object shell.
#
# tinder tests.

import lo
import logging
import os
import random
import sys
import time
import unittest

class Param(lo.Object):

    pass

h = lo.hdl.Handler()
h.walk("losh")
h.start()

try:
    index = int(lo.cfg.args[1])
except:
    index = 1

param = Param()
param.cfg = ["irc", "rss", "krn"]
param.log = ["yo!",]
param.meet = ["test@shell",]
param.rss = ["https://www.reddit.com/r/python/.rss", ""]
param.todo = ["yo!",]

class Event(lo.evt.Event):

    def show(self):
        if lo.cfg.verbose:
            super().show()

class Test_Tinder(unittest.TestCase):

    def test_tinder(self):
        thrs = []
        for x in range(index):
            thrs.append(lo.thr.launch(tests, h))
        for thr in thrs:
            thr.join()

    def test_tinder2(self):
        for x in range(index):
            tests(h)

def consume(elems):
    fixed = []
    for e in elems:
        e.wait()
        fixed.append(e)
    for f in fixed:
        try:
            elems.remove(f)
        except ValueError:
            continue
        
def tests(b):
    events = []
    keys = list(b.cmds)
    random.shuffle(keys)
    for cmd in keys:
        events.extend(do_cmd(h, cmd))
    consume(events)

def do_cmd(b, cmd):
    exs = param.get(cmd, [])
    if not exs:
        exs = ["bla",]
    e = list(exs)
    random.shuffle(e)
    events = []
    for ex in e:
        e = Event()
        e.etype = "command"
        e.orig = repr(h)
        e.origin = "test@shell"
        e.txt = cmd + " " + ex
        e.verbose = lo.cfg.verbose
        h.put(e)
        events.append(e)
    return events
