# LOSH - library object shell.
#
# loader tests.

import lo
import os
import unittest

class Test_Loader(unittest.TestCase):

    def test_loadmod(self):
        l = lo.ldr.Loader()
        l.load_mod("lo.ldr")
        p = l.save()
        ll = lo.ldr.Loader()
        ll.load(p)
        self.assertTrue("lo.ldr" in ll.table)

    def test_loadmod2(self):
        l = lo.ldr.Loader()
        l.load_mod("lo.hdl")
        self.assertTrue("lo.hdl" in l.table)
